﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game1
{
    class Game1
    {        
        private Random _rnd = new Random();
        private int _number;
        public Game1()
        {
            _number = _rnd.Next(0,101);
            //_number = 50;
        }

        public string Comparison(string answer)
        {
            int x = int.MinValue;
            bool wasparsed = int.TryParse(answer, out x);
            string ans = "";
            if (!wasparsed)
                ans = "notnumber";
            else if (x > 100)
                ans = "more100";
            else if (x < 0)
                ans = "less0";
            else if (x == _number)
                ans = "equally";
            else if (x > _number)
                ans = "more";
            else if (x < _number)
                ans = "less";
            return ans;
        }
    }
}
