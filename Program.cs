﻿using System;

namespace Game1
{
    class Program
    {
        static void Main(string[] args)
        {                                              
            var answer = "";
            var counter = new int();
            var x = new int();
            bool wasparsed;
            do
            {
                var game = new Game1();
                counter = 0;
                do
                {
                    Console.Clear();
                    Message("Вас приветствует игра Угадайка", TypeMessage.Blue);
                    Message("1. Start", TypeMessage.Blue);
                    Message("2. Exit", TypeMessage.Blue);
                    wasparsed = int.TryParse(Answer(), out x);
                    if (!wasparsed || x > 2 || x < 0)
                    {
                        Message("Error. Enter 1 or 2", TypeMessage.Red);
                        Message("Type Enter to continu", TypeMessage.Red);
                        Answer();
                    }
                    if (x == 1 || x == 2)
                        break;
                } while (true);
                if (x == 1)
                {
                    do
                    {
                        counter++;
                        x = 0;
                        Console.Clear();
                        Message("Введите число в диапазоне от 0 до 100 включительно", TypeMessage.Blue);
                        answer = Answer();
                        if (game.Comparison(answer) == "equally")
                        {
                            Console.Clear();
                            Message("Поздравляем!!!!", TypeMessage.Blue);
                            Console.WriteLine();
                            do
                            {
                                Message("1. Повторить игру", TypeMessage.Blue);
                                Message("2. Выйти", TypeMessage.Blue);
                                wasparsed = int.TryParse(Answer(), out x);
                                if (!wasparsed)
                                {
                                    Console.Clear();
                                    Message("Error. Enter 1 or 2", TypeMessage.Red);
                                    Message("Type Enter to continu", TypeMessage.Red);
                                    Answer();
                                }
                                if (x < 0 || x > 2)
                                {
                                    Console.Clear();
                                    Message("Error. Enter 1 or 2", TypeMessage.Red);
                                    Message("Type Enter to continu", TypeMessage.Red);
                                    Answer();
                                }
                            } while (x != 1 && x != 2);
                        }
                        if (game.Comparison(answer) == "more")
                        {
                            Console.Clear();
                            Message("Вы ввели большее число", TypeMessage.Red);
                            Message($"осталось попыток{10 - counter}", TypeMessage.White);
                            Message("Type Enter to continu", TypeMessage.White);
                            Answer();
                        }
                        if (game.Comparison(answer) == "less")
                        {
                            Console.Clear();
                            Message("Вы ввели меньшее число", TypeMessage.Red);
                            Message($"осталось попыток{10 - counter}", TypeMessage.White);
                            Message("Type Enter to continu", TypeMessage.White);
                            Answer();
                        }
                        if (game.Comparison(answer) == "more100" || game.Comparison(answer) == "less0" || game.Comparison(answer) == "notnumber")
                        {
                            Console.Clear();
                            Message("Вы ввели число за пределами диапазона или недопустимые символы", TypeMessage.Red);
                            counter--;
                            Message("Type Enter to continu", TypeMessage.White);
                            Answer();
                        }
                        if (x == 1 || x == 2)
                            break;
                        if (counter == 10)
                        {
                            Message("Вы проиграли. Для выхода в главное меню нажмите Enter", TypeMessage.Red);
                            Answer();
                            break;
                        }
                    } while (true);                    
                }
                if(x == 2 )
                    break;
            } while (true);
            return;
        }
        public static string Answer()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            var answer = Console.ReadLine();
            Console.ResetColor();
            return answer;
        }
        public static void Message(string qwestionMassage, TypeMessage type)
        {
            if (type == TypeMessage.Red)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (type == TypeMessage.Blue)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.White;
            }
            Console.WriteLine($"{qwestionMassage}");
            Console.ResetColor();
        }
        internal enum TypeMessage
        {
            Red,
            White,
            Blue
        }
    }
}
